# `Kvantum` sweet theme

Sweet theme for the `Kvantum` QT theme engine made by
[Eliver Lara](https://github.com/EliverLara).

# Install

Copy the contents of [sweet](sweet) into `/usr/share/Kvantum/` and set the theme
in the `Kvantum` manager.
